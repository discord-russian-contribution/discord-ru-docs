# Copyright Notices

Files that belong to their owners must be listed here.

- [src/img/island.gif](src/img/island.gif)
  - © 2016-2018 Discord Inc. — https://discordapp.com/
  - Source: https://dribbble.com/shots/2418189-More-Languages/attachments/468724
- [src/img/languages.gif](src/img/languages.gif)
  - © 2016-2018 Discord Inc. — https://discordapp.com/
  - Source: https://dribbble.com/shots/2569355-Floating-Island
- [src/img/ru-welcome.jpg](src/img/ru-welcome.jpg)
  - © 2016-2018 Discord Inc. — https://discordapp.com/
  - Reconfigured version of `src/img/island.gif` by [DaFri-Nochiterov](https://gitlab.com/DaFri-Nochiterov)
  - Source: https://dribbble.com/shots/2418189-More-Languages/attachments/468724

**Discord Russian Translation Group is not affiliated with Discord Inc.**
