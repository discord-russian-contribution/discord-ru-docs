# Привет! 👋

⚠ Хочу уведомить тебя, что этот проект был передан в руки SDG и теперь доступен в соответствующем репозитории на GitHub.
Это причина, по которой мы архивируем этот проект ЗДЕСЬ и не принимаем больше изменений.
Вы по прежнему можете вносить изменения в репозитории на GitHub и использовать его как хотите, соблюдая лицензию.
Новый проект на GitHub доступен здесь: https://github.com/snowball-dev-group/discord-contrib-ru-docs.

---

# Русский перевод Discord

Это файлы с документацией для проекта по переводу Discord на русский язык.

## О файлах

Документация адаптирована под сайт [@DaFri-Nochiterov](https://gitlab.com/DaFri-Nochiterov), который имеет свой генератор страниц из исходных markdown файлов на Node.js, с использованием библиотеки `markdown-it`.

Список дополнительных вещей:

- Предупреждения и информационные блоки;
- Цитаты;
- Код с подсветкой (используя библиотеку `highlight.js`);
- Различная мета-конфигурация (например, имя пользователя сверху).

### Структура

Это простой проект на [Node.js](https://nodejs.org/) без исполняемых скриптов.

- В папке `src` хранится вся исходная документация
- В папку `out` уходят готовые файлы для сайта

### Построение

Чтобы приготовить исходную документацию, необходимо установить все зависимости через `npm install` (необходимо иметь установленным Node.js).

После можно запустить сборку, используя команду `gulp`.

При сборке сжимаются все изображения, CSS файлы и некоторые другие типы файлов. Ранее также корректировалась мета-информация, но это перестало быть необходимо и каждый файл начал включать собственную мету.

### Копирайты

Мы используем файлы, которые принадлежат их законным владельцам.

Владельцы файлов должны быть включены в список в файле [COPYRIGHT-NOTICES](/COPYRIGHT-NOTICES.md).
