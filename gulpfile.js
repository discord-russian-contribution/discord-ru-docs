const gulp = require("gulp");

gulp.task("build:clean", () => {
	const del = require("del");

	return del(["out/**/*"]);
});

gulp.task("build:clone", () => {
	return gulp
		.src("src/**/*")
		.pipe(gulp.dest('out/'));
});

gulp.task("build:minify", () => {
	const minify = require("gulp-babel-minify");

	return gulp
		.src("out/**/*.js")
		.pipe(minify())
		.pipe(gulp.dest("out/"));
});

gulp.task("build:compress:images", () => {
	const imagemin = require("gulp-imagemin");

	return gulp
		.src("out/**/*{.png,.jpeg,.jpg,.gif,.svg}")
		.pipe(
			imagemin([
				imagemin.gifsicle({
					interlaced: true,
					optimizationLevel: 3
				}),
				imagemin.jpegtran({
					progressive: true
				}),
				imagemin.optipng(),
				imagemin.svgo()
			], {
				verbose: true
			})
		)
		.pipe(gulp.dest("out"));
});

gulp.task("build:compress:css", () => {
	const cleanCSS = require("gulp-clean-css");

	return gulp
		.src("out/**/*.css")
		.pipe(cleanCSS())
		.pipe(gulp.dest("out"));
});

gulp.task("build:compress", gulp.series(["build:compress:images", "build:compress:css"]));

gulp.task("build", gulp.series(["build:clean", "build:clone", "build:compress"]));

gulp.task("default", gulp.series(["build"]));
