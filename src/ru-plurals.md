<!--
{
    "title": "Информация о plural-ах",
    "description": "Описание plural-ов, которые очень часто используются со строками, которые могут меняться в зависимости от числа",
    "author": {
        "firstName": "Sasha",
        "lastName": "Sorokin",
        "email": "dafri.nochiterov8@gmail.com",
        "username": "DaFri_Nochiterov"
    },
    "language": "ru"
}
-->

# Plural-ы

Plural-ы (*русский*. множественное число) — это важная вещь, которая позволяет менять форму слова или предложения в зависимости от количества. Узнать подробнее о множественных числах можно [здесь][wikipedia-plurals].

Пример plural-а - «пользователи»: 1 пользовател**ь**, но 3 пользовател**я**, но 5 пользовател**ей**.

Такой plural выглядит следующим образом после перевода:

```
{users, plural, one {{users} пользователь} few {{users} пользователя} other {{users} пользователей}}
```

Здесь - `{users}` это переменная с числом пользователей и в зависимости от её значения, по ключевым совпадениями мы меняем форму слова «пользователь».

## Совпадения

Перевод в другую форму слова производится в зависимости от числа, для определения формы используются «совпадения» их разделяют на две категории: ключевые и собственные.

### Ключевые совпадения

Чтобы сделать перевод в другую форму немного легче, создатели ICU сделали ключевые совпадения. Такие совпадения регулируются на каждый язык, в английском используется стандартная форма `one-other`, в русском мы имеем форму с четыремя совпадениями `one-few-many-other`.

- `one` (один): любое число которое заканчивается с «один»: «двадцать один», «двести один»;
- `few` (немного): такие числа как 2, 3, 4, 22, и подобные;
- `many` (много): 5-20, 25-30, и подобные;
- `other` (остальное): при отстутствии других совпадений. Plural не имеющий `other` - неверный. **В большинстве случаев `many` заменяется на `other`.**

### Собственные совпадения

Встречается, что в английском языке попадается такой plural:

```
{apps, plural, one {App {app}} other {{apps} apps}} can control your Discord
```

То есть, если `apps` **равняется 1**, то создается дополнительная переменная с названием единственного приложения, которое может управлять Discord. Естественно, перевести используя форму `one` мы не можем, т.к. в русском языке `one` используется не один раз.

Обратите внимание на «равняется 1», это и является нашим «ключевым совпадением». В ICU есть возможность добавить собственное совпадение в plural, для этого необходимо написать `=` и число, чтобы получилось что-то подобное `=1`.

В нашем случае мы будем по совпадению `=1` писать название приложения, `one` будем использовать для остальных чисел, как 21 и т.п.

Результат:

```
{apps, plural, =1 {Приложение {app} может} one {{apps} приложение может} few {{apps} приложения могут} other {{apps} приложений могут}} контроллировать ваш Discord
```

## Форматирование plural-ов

- **Переменная не переводится**
- **`plural` не переводится**
- После запятой ставится пробел `, =1`
- Между совпадением и его блоком ставится пробел `=1 {`
- После каждого (кроме последнего) совпадения ставится пробел `} =`
- Если plural используется для точных значений и ни одно из них не попадает под одно из ключевых совпадений, такое совпадение игнорируется: `{days, plural, one {{days} день} other {{days} дней}}` (1, 7, 30)

**Правильно**: `{users, plural, one {{users} пользователь} few {{users} пользователя} other {{users} пользователей}}`

**Не правильно**:

- `{users, plural, one{{users} пользователь} few{{users} пользователя} other{{users} пользователей} }`
- `{пользователи, множественное число, один{{пользователи} пользователь} другое{{пользователи} пользователей}}`

::: info Не забывайте проверять plural-ы!
Используйте инструменты Crowdin. Они будут находиться ниже поля ввода перевода после того, как вы ввели перевод.
:::

[wikipedia-plurals]:https://ru.wikipedia.org/wiki/%D0%9C%D0%BD%D0%BE%D0%B6%D0%B5%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE